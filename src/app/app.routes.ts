import { Routes } from '@angular/router';
import { MatchListComponent } from './pages/match-list.component';

export const routes: Routes = [
    {
        path: '',
        component: MatchListComponent,
        title: 'Investor Matcher | Front End Code Challenge'
    }
];
