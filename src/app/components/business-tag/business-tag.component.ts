import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-business-tag',
  standalone: true,
  imports: [],
  templateUrl: './business-tag.component.html',
  styleUrl: './business-tag.component.scss'
})
export class BusinessTagComponent {

  @Input() business!: string;
}
