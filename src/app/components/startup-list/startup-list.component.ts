import { Component, EventEmitter, Input, Output } from '@angular/core';
import { InvestorMatched } from '../../interfaces';
import { BusinessTagComponent } from '../business-tag/business-tag.component';

@Component({
  selector: 'app-startup-list',
  standalone: true,
  imports: [BusinessTagComponent],
  templateUrl: './startup-list.component.html',
  styleUrl: './startup-list.component.scss'
})
export class StartupListComponent {

  @Input() record!: InvestorMatched | null;

  @Output() clear = new EventEmitter<void>();
}
