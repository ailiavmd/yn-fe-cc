export enum StorageKeys {
    Investors = 'INVESTORS',
    Startups = 'STARTUPS'
}
