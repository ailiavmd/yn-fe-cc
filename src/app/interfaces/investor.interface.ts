export interface Investor {
    firstName: string;
    business: string;
}
