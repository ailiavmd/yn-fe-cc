import { Startup } from './startup.interface';

export interface InvestorMatched {
    firstName: string;
    business: string;
    startups: Startup[];
}
