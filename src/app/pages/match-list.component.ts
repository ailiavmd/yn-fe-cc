import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DataService } from '../services/data.service';
import { forkJoin } from 'rxjs';
import { Startup, InvestorMatched } from '../interfaces';
import { InvestorFilterPipe } from '../pipes/investor-filter-pipe';
import { FormsModule } from '@angular/forms';
import { StartupListComponent } from '../components/startup-list/startup-list.component';
import { BusinessTagComponent } from '../components/business-tag/business-tag.component';
import { SpinnerComponent } from '../components/spinner/spinner.component';


@Component({
  selector: 'app-match-list',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule, 
    ScrollingModule,
    InvestorFilterPipe,
    StartupListComponent,
    BusinessTagComponent,
    SpinnerComponent
  ],
  templateUrl: './match-list.component.html',
  styleUrl: './match-list.component.scss'
})
export class MatchListComponent implements OnInit {
  /**
   * Filter query
   */
  query!: string;
  /**
   * Currently selected investor, it shows their matched startups
   */
  active!: InvestorMatched | null;
  /**
   * The list of the investors and their matched startups
   */
  matches!: InvestorMatched[];

  constructor(
    private service: DataService
  ) { }

  ngOnInit(): void {
    forkJoin([
      this.service.getInvestors(),
      this.service.getStartups(),
    ]).subscribe(([investors, startups]) => {
      // For every investor
      this.matches = investors.map(inv => {
        const matched: Startup[] = [];
        // Looks for startups that match the investor interest
        for (const s of startups) {
          if (s.business === inv.business || inv.business === 'any') {
            matched.push(s);
            // If it gets to 10 startups, it stops
            if (matched.length === 10) break;
          }
        }
        // Returns the mapped object of the investor and their matches
        return {
          firstName: inv.firstName,
          business: inv.business,
          startups: matched
        };
      });
    });
  }
}
