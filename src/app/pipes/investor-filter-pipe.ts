import { Pipe, PipeTransform } from '@angular/core';
import { InvestorMatched } from '../interfaces';

@Pipe({
  name: 'investorFilter',
  standalone: true
})
export class InvestorFilterPipe implements PipeTransform {
  /**
   * Filters an array of investor matches
   * @param arr the investor array
   * @param query the text query
   * @returns the array filtered after every word in the text query
   */
  transform(arr: InvestorMatched[], query: string): InvestorMatched[] {
    // Validations
    if (!arr || arr.length === 0 || !query || query === '' || query === ' ') { return arr; }
    // create an array of every word in the text query
    const queries = query.split(' ').filter(q => !!q.trim());
    let copy = [ ...arr ];
    // For every word in the text query
    queries.forEach(q => {
      // Filter the remaining array of investors
      copy = copy.filter(inv => {
        // Against each word in the record
        const values = `${inv.firstName} ${inv.business}`.split(' ');
        for (const val of values) {
          if (val.toLowerCase().includes(q.toLowerCase())) return true;
        }
        return false;
      });
    });
    // Return remaining
    return copy;
  }
}
