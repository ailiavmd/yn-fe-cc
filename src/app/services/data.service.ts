import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, iif, map, of, tap } from 'rxjs';
import { Investor, Startup } from '../interfaces';
import { StorageKeys } from '../enums';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient
  ) { }
  /**
   * Parses the csv string to an array
   * @param csv the csv file as text
   * @returns the rows as string arrays
   */
  parse(csv: string): string[][] {
    const rows = csv.split('\n');
    return rows.map(row => row.split(','));
  }
  /**
   * Gets the investor data and maps it
   * @returns an array of the mapped investors
   */
  getInvestors(): Observable<Investor[]> {

    const stored = localStorage.getItem(StorageKeys.Investors);

    return iif(
      // checks the storage to know if we have to send the http request
      () => !!stored,
      of(stored as string),
      this.http.get('/assets/data/investors.csv', { responseType: 'text' })
    ).pipe(
      // save the response to the storage if it was empty
      tap(res => !!!stored && localStorage.setItem(StorageKeys.Investors, res)),
      map(res => {

      const rows = this.parse(res).map(row => {
        const [ firstName, business ] = row;
        return { firstName, business };
      });

      return rows;
    }));   
  }
  /**
   * Gets the startup data and maps it
   * @returns an array of the mapped startups
   */
  getStartups(): Observable<Startup[]> {

    const stored = localStorage.getItem(StorageKeys.Startups);

    return iif(
      // checks the storage to know if we have to send the http request
      () => !!stored,
      of(stored as string),
      this.http.get('/assets/data/startups.csv', { responseType: 'text' })
    ).pipe(
      // save the response to the storage if it was empty
      tap(res => !!!stored && localStorage.setItem(StorageKeys.Startups, res)),
      map(res => {

        const rows = this.parse(res).map(row => {
          const [ name, business ] = row;
          return { name, business };
        });

        return rows;
      }));
  }
}
